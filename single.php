<?php get_header(); ?>
<div class="pt-4">
    <div class="container-custom">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <?php
                    if (have_posts()) : the_post()
                    ?>
                        <?php setPostViews(get_the_ID()); ?>
                        <article>
                            <h1 class="card-title font-weight-bolder my-3"><?php the_title(); ?></h1>
                            <div class="card card-shadow rounded-0 mb-3">
                                <?php if (has_post_thumbnail()) : ?>
                                    <img class="card-img-top rounded-0" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>
                                <div class="card-body">
                                    <div class="post__meta">
                                        <div>
                                            <span class="svg-icon svg-baseline mr-1">
                                                <svg height="484pt" viewBox="-15 -15 484.00019 484" width="484pt" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="m401.648438 18.234375c-24.394532-24.351563-63.898438-24.351563-88.292969 0l-22.101563 22.222656-235.269531 235.144531-.5.503907c-.121094.121093-.121094.25-.25.25-.25.375-.625.746093-.871094 1.121093 0 .125-.128906.125-.128906.25-.25.375-.371094.625-.625 1-.121094.125-.121094.246094-.246094.375-.125.375-.25.625-.378906 1 0 .121094-.121094.121094-.121094.25l-52.199219 156.96875c-1.53125 4.46875-.367187 9.417969 2.996094 12.734376 2.363282 2.332031 5.550782 3.636718 8.867188 3.625 1.355468-.023438 2.699218-.234376 3.996094-.625l156.847656-52.324219c.121094 0 .121094 0 .25-.121094.394531-.117187.773437-.285156 1.121094-.503906.097656-.011719.183593-.054688.253906-.121094.371094-.25.871094-.503906 1.246094-.753906.371093-.246094.75-.621094 1.125-.871094.125-.128906.246093-.128906.246093-.25.128907-.125.378907-.246094.503907-.5l257.371093-257.371094c24.351563-24.394531 24.351563-63.898437 0-88.289062zm-232.273438 353.148437-86.914062-86.910156 217.535156-217.535156 86.914062 86.910156zm-99.15625-63.808593 75.929688 75.925781-114.015626 37.960938zm347.664062-184.820313-13.238281 13.363282-86.917969-86.917969 13.367188-13.359375c14.621094-14.609375 38.320312-14.609375 52.945312 0l33.964844 33.964844c14.511719 14.6875 14.457032 38.332031-.121094 52.949218zm0 0" />
                                                </svg>
                                            </span> <?php the_author(); ?>
                                        </div>
                                        <div class="ml-2">
                                            <span class="svg-icon svg-baseline mr-1">
                                                <svg height="384pt" viewBox="0 0 384 384" width="384pt" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="m343.59375 101.039062c-7.953125 3.847657-11.28125 13.417969-7.433594 21.367188 10.511719 21.714844 15.839844 45.121094 15.839844 69.59375 0 88.222656-71.777344 160-160 160s-160-71.777344-160-160 71.777344-160 160-160c36.558594 0 70.902344 11.9375 99.328125 34.519531 6.894531 5.503907 16.976563 4.351563 22.480469-2.566406 5.503906-6.914063 4.351562-16.984375-2.570313-22.480469-33.652343-26.746094-76-41.472656-119.238281-41.472656-105.863281 0-192 86.136719-192 192s86.136719 192 192 192 192-86.136719 192-192c0-29.335938-6.40625-57.449219-19.039062-83.527344-3.839844-7.96875-13.441407-11.289062-21.367188-7.433594zm0 0" />
                                                    <path d="m192 64c-8.832031 0-16 7.167969-16 16v112c0 8.832031 7.167969 16 16 16h80c8.832031 0 16-7.167969 16-16s-7.167969-16-16-16h-64v-96c0-8.832031-7.167969-16-16-16zm0 0" />
                                                </svg>
                                            </span> <?php the_date(); ?>
                                        </div>
                                        <div class="ml-2">
                                            <?php edit_post_link(__('Edit'), ''); ?>
                                        </div>
                                    </div>
                                    <div class="text-justify my-4 font-17px" style="line-height: 1.8;">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <!-- End of article -->
                    <?php
                    else :
                        get_template_part('partials/no', 'content');
                    endif;
                    ?>
                </div>


                <!-- Single Page Below News  Ad -->
                <?php
                $ad_location = 'single-page-below-news';
                set_query_var('ad_location', $ad_location);
                get_template_part('partials/ad/full-width', 'banner');
                ?>
                <!-- End of Single Page Below News  Ad -->

            </div>
            <div class="col-md-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>
<div class="container-custom py-4">
    <?php
    $prev_post = get_previous_post();
    $next_post = get_next_post();
    ?>

    <div class="d-block d-md-flex font-16px">
        <?php if (get_previous_post()) : ?>
            <a href="<?php echo get_permalink($prev_post->ID); ?>" class="d-flex simple-pagination mb-3 mb-md-0 ml-0 ml-md-n3">
                <div class="pagination-arrow arrow-left mr-2">
                    <svg fill="none" viewBox="0 0 31.344 105.69" stroke="currentColor" stroke-width="3px" fill-rule="evenodd">
                        <path d="M29.844 2.86l-25 50 25 50"></path>
                    </svg>
                </div>
                <div class="align-self-center">
                    <div class="h5-responsive title"><?php echo apply_filters('the_title', $prev_post->post_title); ?></div>
                    <div class="text-left date"><?php echo get_the_date(); ?></div>
                </div>
            </a>
        <?php endif; ?>
        <?php if (get_next_post()) : ?>
            <a href="<?php echo get_permalink($next_post->ID); ?>" class="ml-auto d-flex simple-pagination text-right justify-content-end mr-0 mr-md-n3">
                <div class="align-self-center">
                    <div class="h5-responsive title"><?php echo apply_filters('the_title', $next_post->post_title); ?></div>
                    <div class="text-right date"><?php echo get_the_date(); ?></div>
                </div>
                <div class="pagination-arrow arrow-right ml-2">
                    <svg id="icon-arrow-thin-right" fill="none" viewBox="0 0 30.69 103" stroke="currentColor" stroke-width="3px" fill-rule="evenodd" class="__web-inspector-hide-shortcut__">
                        <path d="M4.19 1.51l25 50-25 50"></path>
                    </svg>
                </div>
            </a>
        <?php endif; ?>
    </div>
</div>
<!-- End of container -->
<?php get_footer(); ?>