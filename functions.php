<?php
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
require_once get_template_directory() . '/includes/enqueue-scripts.php';
require_once get_template_directory() . '/includes/facebook-og-tags.php';
require_once get_template_directory() . '/includes/customize-admin-panel.php';
require_once get_template_directory() . '/includes/customize-dashboard.php';
require_once get_template_directory() . '/includes/post-views.php';
require_once get_template_directory() . '/includes/customizer/site_settings_customizer.php';
require_once get_template_directory() . '/includes/customizer/frontpage_customizer.php';
require_once get_template_directory() . '/includes/custom-loop.php';
require_once get_template_directory() . '/includes/widgets/theme_widgets.php';
require_once get_template_directory() . '/includes/nepali_calendar.php';
require_once get_template_directory() . '/includes/site_settings.php';

function wp_theme_setup()
{
	add_theme_support('post-thumbnails');

	register_nav_menus(array(
		'primary' => __('Primary Menu', 'Ranatharu'),
		'footer-menu-one' => __('Footer Menu One', 'Ranatharu'),
		'footer-menu-two' => __('Footer Menu Two', 'Ranatharu'),
		'footer-menu-three' => __('Footer Menu Three', 'Ranatharu'),
	));
}
add_action('after_setup_theme', 'wp_theme_setup');

function themename_custom_logo_setup()
{
	$defaults = array(
		'height'      => 120,
		'width'       => 200,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array('site-title', 'site-description'),
	);
	add_theme_support('custom-logo', $defaults);
}
add_action('after_setup_theme', 'themename_custom_logo_setup');


/**
 * Create Category Link
 *
 * @param  mixed $slug
 * @return void
 */
function cat_link($slug)
{
	echo 'https://example.com';
}

/**
 * generate query for particular category
 *
 * @param  mixed $slug
 * @param  mixed $posts_per_page
 * @return void
 */
function cat_query($slug, $posts_per_page = 1)
{
	$query = array(
		'category_name' => $slug,
		'posts_per_page' => $posts_per_page,
	);
	return new WP_Query($query);
}

/**
 * my_excerpt_length
 *
 * @param  mixed $length
 * @return void
 */
function custom_excerpt_length($length)
{
	return 30;
}
add_filter('excerpt_length', 'custom_excerpt_length');

function custom_excerpt_more($more)
{
	return ' ';
}
add_filter('excerpt_more', 'custom_excerpt_more');

// Remove <p> tag from excerpt
remove_filter( 'the_excerpt', 'wpautop' );

// to replace Howdy to Welcome

add_filter('admin_bar_menu', 'replace_wordpress_howdy', 25);
function replace_wordpress_howdy($wp_admin_bar)
{
    $my_account = $wp_admin_bar->get_node('my-account');
    $newtext = str_replace('Howdy,', 'Hello,', $my_account->title);
    $wp_admin_bar->add_node(array(
        'id' => 'my-account',
        'title' => $newtext,
    ));
}