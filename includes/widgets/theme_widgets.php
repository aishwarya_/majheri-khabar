<?php
function init_theme_widgets() {

	// register_sidebar( array(
	// 	'name'          => 'Header Right',
	// 	'id'            => 'header-right',
	// 	'description'	=> 'The content will be shown in the right section of header.',
	// 	'before_widget' => '<div>',
	// 	'after_widget'  => '</div>',
	// 	'before_title'  => '<h2>',
	// 	'after_title'   => '</h2>',
	// ) );

	// register_sidebar( array(
	// 	'name'          => 'Below Featured',
	// 	'id'            => 'below-featured',
	// 	'description'	=> 'The content will be shown below featured news section in homepage.',
	// 	'before_widget' => '<div class="text-center my-3">',
	// 	'after_widget'  => '</div>',
	// 	'before_title'  => '<h2>',
	// 	'after_title'   => '</h2>',
	// ) );

	// register_sidebar( array(
	// 	'name'          => 'Below Politics',
	// 	'id'            => 'below-politics',
	// 	'description'	=> 'The content will be shown below politics news section in homepage.',
	// 	'before_widget' => '<div class="text-center my-3">',
	// 	'after_widget'  => '</div>',
	// 	'before_title'  => '<h2>',
	// 	'after_title'   => '</h2>',
	// ) );

	// register_sidebar( array(
	// 	'name'          => 'Sidebar Top',
	// 	'id'            => 'sidebar-top',
	// 	'description'	=> 'The content will be shown at the top of sidebar.',
	// 	'before_widget' => '<div class="text-center mb-3">',
	// 	'after_widget'  => '</div>',
	// 	'before_title'  => '<h2>',
	// 	'after_title'   => '</h2>',
	// ) );

	// register_sidebar( array(
	// 	'name'          => 'Sidebar Bottom',
	// 	'id'            => 'sidebar-bottom',
	// 	'description'	=> '',
	// 	'before_widget' => '<div class="text-center my-3">',
	// 	'after_widget'  => '</div>',
	// 	'before_title'  => '<h2>',
	// 	'after_title'   => '</h2>',
	// ) );

	register_sidebar( array(
		'name'          => 'Footer Description',
		'id'            => 'footer-description',
		'description'	=> 'The content of this widget will be shown in the top left corner of footer below logo.',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Footer Center',
		'id'            => 'footer-center',
		'description'	=> 'The content of this widget will be shown in the center column of footer.',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'init_theme_widgets' );