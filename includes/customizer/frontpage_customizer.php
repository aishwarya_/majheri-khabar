<?php
function front_page_customize( $wp_customize ) {
    require_once get_stylesheet_directory() . '/includes/customizer-controls/dropdown-category.php';

	$wp_customize->add_section( 'front_page_settings', array(
		'title' => __( 'Front Page' ),
		'priority' => 102
	) );

}
add_action( 'customize_register', 'front_page_customize' );
