<?php
function theme_enqueue_scripts()
{
	wp_enqueue_style('Noto_Sans_font', 'https://fonts.googleapis.com/css?family=Noto+Sans&display=swap');
	// wp_enqueue_style('Font_Awesome', 'https://use.fontawesome.com/releases/v5.6.1/css/all.css');
	wp_enqueue_style('Bootstrap_css', get_template_directory_uri() . '/mdb/css/bootstrap.min.css');
	wp_enqueue_style('MDB', get_template_directory_uri() . '/mdb/css/mdb.min.css');
	wp_enqueue_style('Style', get_template_directory_uri() . '/css/style.css', array(), '1.1.0');
	wp_enqueue_style('Utilities', get_template_directory_uri() . '/css/utilities.css');
	wp_enqueue_style('Cards_css', get_template_directory_uri() . '/css/cards.css');
	wp_enqueue_style('Media_css', get_template_directory_uri() . '/css/media.css');
	wp_enqueue_style('Pagination_css', get_template_directory_uri() . '/css/pagination.css');

	wp_enqueue_script('jQuery', get_template_directory_uri() . '/mdb/js/jquery-3.4.1.min.js', array(), '3.3.1', true);
	wp_enqueue_script('Tether', get_template_directory_uri() . '/mdb/js/popper.min.js', array(), '1.0.0', true);
	wp_enqueue_script('Bootstrap', get_template_directory_uri() . '/mdb/js/bootstrap.min.js', array(), '1.0.0', true);
	wp_enqueue_script('MDB', get_template_directory_uri() . '/mdb/js/mdb.min.js', array(), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
