<?php
$location = get_query_var('ad_location');
$margin = get_query_var('margin');
$ad_query = array(
    'post_type' => 'advertisement',
    'meta_query'    => array(
        array(
            'key'       => 'location',
            'value'     => '"' . $location . '"',
            'compare'   => 'LIKE'
        ),
    )
);
$ad_query = new WP_Query($ad_query);
if ($ad_query->have_posts()) {
    foreach (wp_loop($ad_query) as $post) {
?>
        <div class="container-custom <?= $margin != null ? $margin : 'my-3'; ?>">
            <?php $has_link = get_field('link') != null ? true : false; ?>
            <?php if ($has_link) : ?>
                <a href="<?php echo get_field('link'); ?>" target="_blank">
                <?php endif; ?>
                <img class="img-fluid" src="<?php echo get_field('image'); ?>" alt="" style="width: 100%;">
                <?php if ($has_link) : ?>
                </a>
            <?php endif;; ?>
        </div>
<?php
    }
}
?>