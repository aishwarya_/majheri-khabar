<?php echo get_header(); ?>

<div class="my-4"></div>

<div class="container-custom">
    <div class="row">
        <div class="col-md-9">
            <!-- Featured -->
            <?php
            $query = cat_query('featured', 1);
            foreach (wp_loop($query) as $post) {
                array_push($exclude, get_the_ID());
            ?>
            <?php get_template_part('components/post', 'card-big'); ?>
            <?php }; ?>

            <!-- Province -->
            <?php $section_slug = 'province'; ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="category__header">
                        <div class="category__title"><a href="<?php cat_link($section_slug); ?>">प्रदेश</a></div>
                        <div class="category__link"><a href="<?php cat_link($section_slug); ?>">थप</a></div>
                    </div>
                    <div>
                        <?php
                        $query = cat_query($section_slug, 4);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                        ?>
                        <?php get_template_part('components/post', 'card'); ?>
                        <?php }; ?>
                    </div>
                </div>
                <div class="col-md-8">
                    <?php
                        $query = cat_query($section_slug, 2);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                    ?>
                    <?php get_template_part('components/post', 'card-big'); ?>
                    <?php }; ?>
                </div>
            </div>
            <!-- End of Province -->

        </div>
        <div class="col-md-3">
            <!-- Local -->
            <?php $section_slug = 'local'; ?>
            <div class="category__header">
                <div class="category__title"><a href="<?php cat_link($section_slug); ?>">स्थानीय समाचार</a></div>
                <div class="category__link"><a href="<?php cat_link($section_slug); ?>">थप</a></div>
            </div>
            <div>
                <?php
                        $query = cat_query($section_slug, 4);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                    ?>
                <?php get_template_part('components/post', 'card'); ?>
                <?php }; ?>
            </div>
            <!-- End of Local -->

        </div>
    </div>

    <!-- International and National -->
    <div class="row">
        <!-- International -->
        <?php $section_slug = 'international'; ?>
        <div class="col-md-4">
            <div class="category__header">
                <div class="category__title"><a href="<?php cat_link($section_slug); ?>">अन्तराष्टिय</a></div>
                <div class="category__link"><a href="<?php cat_link($section_slug); ?>">थप</a></div>
            </div>
            <div>
                <?php
                        $query = cat_query($section_slug, 5);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                    ?>
                <?php get_template_part('components/post', 'media'); ?>
                <?php }; ?>
            </div>
        </div>
        <!-- End of International -->

        <!-- National -->
        <?php $section_slug = 'national'; ?>
        <div class="col-md-4">
            <div class="category__header">
                <div class="category__title"><a href="<?php cat_link($section_slug); ?>">राष्टिय</a></div>
                <div class="category__link"><a href="<?php cat_link($section_slug); ?>">थप</a></div>
            </div>
            <div class="row">
                <?php
                        $query = cat_query($section_slug, 2);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                    ?>
                <div class="col-md-12">
                    <?php get_template_part('components/post', 'card'); ?>
                </div>
                <?php }; ?>
            </div>
        </div>
        <div class="col-md-4">
          <!-- Weather -->
          <?php $section_slug = 'weather'; ?>
            <div class="category__header">
                <div class="category__title"><a href="<?php cat_link($section_slug); ?>">माैसम समाचार</a></div>
                <div class="category__link"><a href="<?php cat_link($section_slug); ?>">थप</a></div>
            </div>
            <div>
                <?php
                        $query = cat_query($section_slug, 5);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                    ?>
                <?php get_template_part('components/post', 'media'); ?>
                <?php }; ?>
            </div>
            <!-- End of Weather -->
        </div>
        
        <!-- End of National -->
    </div>
    <!-- End of International and National -->

    <!-- Economic -->
    <?php $section_slug = 'economic'; ?>
    <div>
        <div class="category__header">
            <div class="category__title"><a href="<?php cat_link($section_slug); ?>">अर्थ</a></div>
            <div class="category__link"><a href="<?php cat_link($section_slug); ?>">थप</a></div>
        </div>
        <div class="row">
            <?php
                        $query = cat_query($section_slug, 4);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                            $count = isset($count) ? ++$count : 1;
                    ?>
            <?php  if($count == 1): ?>
            <div class="col-md-6">
                <?php get_template_part('components/post', 'card-big'); ?>
            </div>
            <?php else: ?>
            <?php if($count == 2): ?>
            <div class="col-md-6">
                <?php endif; ?>
                <?php get_template_part('components/post', 'media');  ?>
                <?php if($count == 4): ?>
            </div>
            <?php endif; ?>
            <?php endif; ?>
            <?php }; ?>
        </div>
    </div>
    <!-- End of Economic -->

    <!-- Culture and Thoughts -->
    <div class="row">
        <!-- Culture -->
        <?php $section_slug = 'culture'; ?>
        <div class="col-md-6">
            <div class="category__header">
                <div class="category__title"><a href="<?php cat_link($section_slug); ?>">सस्कृति</a></div>
                <div class="category__link"><a href="<?php cat_link($section_slug); ?>">थप</a></div>
            </div>
            <div class="row">
                <?php
                        $query = cat_query($section_slug, 4);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                    ?>
                <div class="col-md-6" style="margin-bottom: 75px;">
                    <?php get_template_part('components/post', 'card-alt'); ?>
                </div>
                <?php }; ?>
            </div>
        </div>
        <!-- End of Culture -->
        <!-- Thoughts -->
        <?php $section_slug = 'thoughts'; ?>
        <div class="col-md-6">
            <div class="category__header">
                <div class="category__title"><a href="<?php cat_link($section_slug); ?>">विचार</a></div>
                <div class="category__link"><a href="<?php cat_link($section_slug); ?>">थप</a></div>
            </div>
            <div class="row">
                <?php
                        $query = cat_query($section_slug, 4);
                        foreach (wp_loop($query) as $post) {
                            array_push($exclude, get_the_ID());
                    ?>
                <div class="col-md-6 mb-4">
                    <?php get_template_part('components/post', 'card-alt'); ?>
                </div>
                <?php }; ?>
            </div>
        </div>
        <!-- End of Thoughts -->
    </div>
    <!-- Culture and Thoughts -->

</div>
<!-- End of custom container -->

<?php echo get_footer(); ?>