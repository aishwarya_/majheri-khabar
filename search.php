<?php
get_header();
global $wp_query;
?>
<div class="light-blue lighten-5 pt-4">
    <div class="container pb-3">

        <div class="mb-3">
            <?php get_search_form(); ?>
        </div>

        <h4 class="h4-responsive text-dark font-mont font-weight-bolder my-3">
            <?php echo $wp_query->found_posts; ?>
            <?php _e('Search results found for', 'locale'); ?>: "<?php the_search_query(); ?>"
        </h4>

        <?php if (have_posts()) { ?>
            <div class="row">
                <?php while (have_posts()) {
                    the_post(); ?>
                    <div class="col-md-4 d-flex">
                        <?php get_template_part('partials/post', 'card'); ?>
                    </div>
                <?php } ?>
            </div>
            <!-- Pagination -->
            <?php get_template_part('partials/page', 'links'); ?>

        <?php } ?>

    </div>
</div>
</div>
<?php get_footer(); ?>