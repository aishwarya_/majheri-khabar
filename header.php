<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <!-- Required meta tags always come first -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>
        <?php bloginfo('name'); ?> |
        <?php is_front_page() ? bloginfo('description') : wp_title(); ?>
    </title>
    <?php wp_head(); ?>
</head>

<body class="white">

    <?php
    global $exclude;
    $exclude = array();
    ?>

    <header class="container-custom d-block d-md-flex py-3">
        <div class="site-logo align-self-center mr-3">
            <a href="<?php echo esc_url(home_url()); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-square-original.jpg" alt="<?php bloginfo('name'); ?>" style="max-height: 100px;">
            </a>
        </div>
        <style>
            @keyframes blinking {
                0% {
                    color: var(--theme-color);
                }

                40% {
                    opacity: .8;
                    color: #209c2c;
                }

                80% {
                    opacity: .9;
                    color: #ec2025;
                }
            }

            .tagline {
                display: block;
                font-size: 1.5em;
                font-weight: 600;
                color: var(--theme-color);
                font-style: italic;
                line-height: 1;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                animation: blinking 4s infinite;
            }

            .brand-name {
                display: block;
                font-size: 2.5em;
                font-weight: 700;
                color: #209c2c;
                line-height: 1.2;
                /* font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"; */
            }
        </style>
        <div class="my-3 my-md-0 mx-auto text-center">
            <a class="tagline" href="<?php echo esc_url(home_url()); ?>">अन्तर मनको आवाज</a>
            <a class="brand-name" href="<?php echo esc_url(home_url()); ?>">रेडियो मनको मझेरी ९१ मेगाहर्ज</a>
        </div>

        <div class="ml-auto align-self-center d-none d-md-flex">

            <?php if (has_custom_logo()) :
                $custom_logo_id = get_theme_mod('custom_logo');
                $image = wp_get_attachment_image_src($custom_logo_id, 'large');
            ?>
                <a href="<?php echo esc_url(home_url()); ?>">
                    <img src="<?php echo $image['0']; ?>" alt="<?php bloginfo('name'); ?>" style="max-height: 100px;">
                </a>
            <?php endif ?>

            <!-- Header Right Widget -->
            <div class="ml-auto">
                <?php
                $ad_location = 'header-right';
                set_query_var('ad_location', $ad_location);
                get_template_part('partials/ad/full-width', 'banner');
                ?>
            </div>
            <!-- End of Header Right Widget -->

        </div>
    </header>

    <header class="sticky-top">
        <div id="navigationMenu">
            <div class="container-custom">
                <nav class="navbar navbar-expand-lg navbar-dark z-depth-0 p-0">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarMenu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <?php
                    wp_nav_menu(array(
                        'theme_location'  => 'primary',
                        'depth'           => 2,
                        'container'       => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id'    => 'navbarMenu',
                        'menu_class'      => 'navbar-nav',
                        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'          => new WP_Bootstrap_Navwalker(),
                    ));
                    ?>
                </nav>
            </div>
        </div>
    </header>