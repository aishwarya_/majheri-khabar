<?php get_header(); ?>
<div class="blue-grey lighten-5">
    <div class="container-custom">
        <div class="page__heading">
            <h1 class="title">
                <?php echo $wp_query->post->post_title; ?>
        </div>
    </div>
</div>
<div class="container-custom py-4">
    <?php
    if (have_posts()) : the_post();
        echo the_content();
    endif;
    ?>
</div>
<?php get_footer(); ?>