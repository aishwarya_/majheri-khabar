<div class="">
   <!-- Sidebar Top Ad -->
   <?php
    $ad_location = 'sidebar-top';
    set_query_var('ad_location', $ad_location);
    set_query_var('margin', 'mb-3');
    get_template_part('partials/ad/full-width', 'banner');
    ?>
    <!-- End of Sidebar Top Ad -->

	<div class="category__header">
		<div class="category__title">Latest News</div>
	</div>
	<?php
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 6,
		'post__not_in' => array(get_the_ID()),
	);
	$featured = new WP_Query($args);
	if ($featured->have_posts()) {
		while ($featured->have_posts()) : $featured->the_post();
	?>
			<?php get_template_part('components/post', 'media-simple'); ?>
	<?php
		endwhile;
		wp_reset_postdata();
	}
	?>

    <!-- Sidebar Bottom Ad -->
    <?php
    $ad_location = 'sidebar-bottom';
    set_query_var('ad_location', $ad_location);
    set_query_var('margin', 'my-3');
    get_template_part('partials/ad/full-width', 'banner');
    ?>
    <!-- End of Sidebar Bottom Ad -->

	<!-- Facebook Page Iframe -->
	<?php get_template_part('partials/facebook', 'page'); ?>
</div>