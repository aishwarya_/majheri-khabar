<div class="post__media media__simple">
    <div class="row">
        <div class="col-4 pr-0">
            <a href="<?php the_permalink(); ?>">
                <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url(null, 'thumbnail'); ?>" alt="<?php the_title(); ?>">
            </a>
        </div>
        <div class="col-8">
            <h6 class="media__title mt-0">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h6>
        </div>
    </div>
</div>