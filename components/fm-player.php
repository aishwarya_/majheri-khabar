<style>
    #live-radio {
        position: fixed;
        right: 10px;
        bottom: 10px;
        z-index: 999;
    }

    .fm-wrapper {
        position: relative;
        background-color: var(--theme-color);
        padding-right: 20px;
        color: #fff;
        border-top-right-radius: 2.5rem;
        border-bottom-right-radius: 2.5rem;
        border-radius: 2.5rem;
    }

    #live-radio .fm-icon-wrapper {
        font-size: 3rem;
    }

    #live-radio .title {
        padding-left: 15px;
    }
</style>
<div id="live-radio">
    <audio id="fm-player" src="http://live.itech.host:2620/stream"></audio>
    <div class="fm-wrapper d-flex">
        <div id="fm-button" class="align-self-center d-flex">
            <span class="svg-icon fm-icon-wrapper">
                <svg version="1.1" id="fm-play-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                    <g>
                        <path d="M437.019,74.98C388.667,26.628,324.38,0,256,0C187.619,0,123.332,26.629,74.98,74.98C26.629,123.332,0,187.62,0,256
                    c0,68.381,26.629,132.668,74.98,181.02C123.332,485.371,187.619,512,256,512c68.38,0,132.667-26.629,181.019-74.98
                    C485.371,388.667,512,324.38,512,256S485.371,123.333,437.019,74.98z M400.887,268.482L208.086,397.014
                    c-2.51,1.674-5.411,2.52-8.321,2.52c-2.427,0-4.859-0.588-7.077-1.775c-4.877-2.609-7.922-7.693-7.922-13.225V127.467
                    c0-5.532,3.045-10.615,7.922-13.225c4.876-2.61,10.795-2.325,15.398,0.745L400.887,243.52c4.173,2.782,6.68,7.466,6.68,12.481
                    C407.566,261.017,405.06,265.699,400.887,268.482z" />
                    </g>
                </svg>
                <svg id="fm-pause-icon" style="display: none;" viewBox="0 0 511.448 511.448">
                    <path d="m436.508 74.94c-99.913-99.913-261.64-99.927-361.567 0-99.913 99.913-99.928 261.64 0 361.567 99.913 99.913 261.64 99.928 361.567 0 99.912-99.912 99.927-261.639 0-361.567zm-202.451 255.45c0 11.598-9.402 21-21 21s-21-9.402-21-21v-149.333c0-11.598 9.402-21 21-21s21 9.402 21 21zm85.333 0c0 11.598-9.402 21-21 21s-21-9.402-21-21v-149.333c0-11.598 9.402-21 21-21s21 9.402 21 21z" />
                </svg>
            </span>
        </div>
        <div class="title align-self-center">Listen Live</div>
    </div>
</div>

<script>
    var fmPlayer = document.getElementById('fm-player');
    var fmButton = document.getElementById('fm-button');
    var fmPlayIcon = document.getElementById('fm-play-icon');
    var fmPauseIcon = document.getElementById('fm-pause-icon');
    var playFm = function() {
        fmPlayer.play();
        fmPlayIcon.style.display = "none";
        fmPauseIcon.style.display = "block";
    }
    var pauseFm = function() {
        fmPlayer.pause();
        fmPlayIcon.style.display = "block";
        fmPauseIcon.style.display = "none";
    }
    fmButton.addEventListener('click', function() {
        if (fmPlayer.paused) {
            console.log('Playing FM');
            playFm();
        } else {
            console.log('Pausing FM');
            pauseFm();
        }
    });
</script>