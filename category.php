<?php get_header(); ?>
<?php
$obj = get_queried_object();
?>
<div class="blue-grey lighten-5">
    <div class="container-custom">
        <?php if ($obj->name) : ?>
            <div class="page__heading">
                <h1 class="title"><?php echo $obj->name; ?></h1>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="container-custom">
    <div class="py-5">
        <div class="row">
            <?php
            $paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
            $filter = array(
                'post_type'    => 'post',
                'cat' => $obj->term_id,
                'order' => 'desc',
                'paged' => $paged,
            );
            $query = new WP_Query($filter);
            ?>
            <?php if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
            ?>
                    <div class="col-md-4 mb-4">
                        <?php get_template_part('components/post', 'card-alt'); ?>
                    </div>
                <?php
                }
                ?>
            <?php
                wp_reset_postdata();
            } else { ?>
                <?php get_template_part('partials/no', 'content'); ?>
            <?php } ?>
        </div>

        <!-- Pagination -->
        <?php get_template_part('partials/page', 'links'); ?>

    </div>
</div>
<?php get_footer(); ?>